/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author THANAWAT_TH
 */
public class TestUpdateProduct {

    public static void main(String[] args) {

        Connection conn = null;
        PreparedStatement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            //Process
            String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?";

            stmt = conn.prepareStatement(sql);
            stmt.setString(1, "Latae2");
            stmt.setDouble(2, 130);
            stmt.setInt(3, 3);
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);

            //ResultSet result = stmt.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();

    }
}
