/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author THANAWAT_TH
 */
public class TestSelectProduct {

    public static void main(String[] args) {

        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            //Process
            String sql = "SELECT id,name,price FROM product";
            stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                
                Product Product = new Product(id,name,price);
                System.out.println(Product);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();

    }
}
