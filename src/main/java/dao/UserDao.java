/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.thanawat.storeproject.poc.TestInsertUser;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author THANAWAT_TH
 */
public class UserDao implements DaoInterface<User>{

    @Override
    public int add(User object) {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        
        try {
            String sql = "INSERT INTO user (name,tel,password ) VALUES (?,?,?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPassword());   
            int row = stmt.executeUpdate();           
            ResultSet result = stmt.getGeneratedKeys();

            if(result.next()){
               id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestInsertUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList<User> list = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        
        try {
            String sql = "SELECT id,name,tel,password FROM user";
            stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            
            while(result.next()){
                
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(id,name,tel,password);
                System.out.println(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        User user = null;
                
        try {
            String sql = "SELECT id,name,tel,password FROM user WHERE id= " + id;
            stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            
            if(result.next()){
                
                int uid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                user = new User(uid,name,tel,password);
               
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return user;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        
            //Process          
        try {
            String sql = "DELETE FROM user WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
            

        

        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();       
        int row = 0;
        
        try {
            //Process
            String sql = "UPDATE user SET name = ?,tel = ? ,password = ? WHERE id = ?";

            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPassword());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();          

            
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
    
    public static void main(String[] args) {
        UserDao dao = new UserDao();

        int id = dao.add(new User(-1,"Emi Fukuda","083-333-3333","HeaTun"));
        System.out.println(id);
        User lastuser = dao.get(id);
        System.out.println("Last user : " + lastuser);
        lastuser.setPassword("HeaBoy");
        System.out.println(lastuser);
        dao.update(lastuser);
        System.out.println(dao.get(id));
        User updateUser = dao.get(id);
        System.out.println("Update Product : " + updateUser);
        dao.delete(id);
        User Deleteuser = dao.get(id);
        System.out.println("Delete User : " + Deleteuser);
    }
}
