/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;


/**
 *
 * @author THANAWAT_TH
 */
public class CustomerDao implements DaoInterface<Customer>{

    @Override
    public int add(Customer object) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        
        try {
            String sql = "INSERT INTO customer (name,tel ) VALUES (?,?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());              
            int row = stmt.executeUpdate();           
            ResultSet result = stmt.getGeneratedKeys();              
            if(result.next()){
               id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList<Customer> list = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "SELECT id,name,tel FROM customer";
            stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            
            while(result.next()){
                
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                
                Customer customer = new Customer(id,name,tel);
                System.out.println(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        Customer customer = null;
                

            
        try {
            String sql = "SELECT id,name,tel FROM customer WHERE id= " + id;
            stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            
            if(result.next()){
                
                int cid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");               
                customer = new Customer(cid,name,tel);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return customer;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;


            //Process
            String sql = "DELETE FROM customer WHERE id = ?";
        try {
            
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();       
        int row = 0;
        
        try {
            //Process
            String sql = "UPDATE customer SET name = ?,tel = ? WHERE id = ?";

            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();          

            
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();

        int id = dao.add(new Customer(-1,"Emi Fukuda","083-333-3333"));
        System.out.println(id);
        Customer lastCustomer = dao.get(id);
        System.out.println("Last user : " + lastCustomer);
        lastCustomer.setName("Sora Aki");
        System.out.println(lastCustomer);
        dao.update(lastCustomer);
        System.out.println(dao.get(id));
        Customer updateCustomer = dao.get(id);
        System.out.println("Update Product : " + updateCustomer);
        dao.delete(id);
        Customer DeleteCustomer = dao.get(id);
        System.out.println("Delete User : " + DeleteCustomer);
    }
}
